 provider "aws" {
    version = "2.69.0"
    region = "US-East-2"
}

resource "aws_instance" "Terraform-Test" {
 ami                    = "ami-0f7919c33c90f5b58"
 instance_type          = "t2.micro"
 vpc_security_group_ids = [aws_security_group.nginx-sg.id]

 tags = {
   Name = "Terraform Build"
 }
}